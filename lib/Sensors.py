from paramiko.client import SSHClient, AutoAddPolicy


class Sensors:
    SOCI2CDEV = "0"
    SOCEEPROMADDRHEX = "52"

    RTCI2CDEV = "1"
    RTCADDRHEX = "57"

    BOARDI2CDEV = "1"
    SHTADDRHEX = "40"

    ACCLMTRI2CDEV = "0"
    ACCLMTRHEX = "68"

    HUMIDITYI2CDEV = "0"
    HUMIDITYHEX = "70"

    BOARDEEPROMADDRHEX = "54"
    DEFAULTRS485UARTSETTINGS = "1:0:8bd:0:3:1c:7f:15:4:5:1:0:11:13:1a:0:12:f:17:16:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0"
    IO_FUNCTION_BUTTON = "134"
    IO_OPT1 = "110"
    IO_OPT2 = "112"
    IO_OPT3 = "123"
    IO_OPT4 = "124"
    NETDEVICE = "eth0"
    PINGSERVER = "8.8.8.8"
    BUTTONTESTSECS = 10

    def __init__(self, remote_ip, key):
        self.coefficient = 100 / 80
        self.ssh = SSHClient()
        self.ssh.set_missing_host_key_policy(AutoAddPolicy())
        self.ssh.connect(remote_ip, username="root", key_filename=key)

    def get_humidity(self):
        command = 'cat  /sys/class/i2c-dev/i2c-'+self.SOCI2CDEV+'/device/'+self.SOCI2CDEV+'-00'+self.HUMIDITYHEX+'/hwmon/hwmon*/humidity1_input'
        print(command)
        stdin, stdout, stderr = self.ssh.exec_command(command)

        serr = stderr.read()
        if serr:
            raise Exception(serr)

        sout = float(stdout.read())
        return sout

    def get_temperature(self):
        command = 'cat  /sys/class/i2c-dev/i2c-'+self.SOCI2CDEV+'/device/'+self.SOCI2CDEV+'-00'+self.HUMIDITYHEX+'/hwmon/hwmon*/temp1_input'
        print(command)
        stdin, stdout, stderr = self.ssh.exec_command(command)

        serr = stderr.read()
        if serr:
            raise Exception(serr)

        sout = float(stdout.read())
        return sout

    def get_proximity(self):
        command = 'cat  /sys/bus/iio/devices/iio:\device*/in_proximity_raw'
        stdin, stdout, stderr = self.ssh.exec_command(command)

        serr = stderr.read()
        if serr:
            raise Exception(serr)

        sout = float(stdout.read())
        return sout

    def get_accelerometer(self):
        command = 'cat /sys/bus/iio/devices/iio:device*/in_accel_x_raw'
        stdin, stdout, stderr = self.ssh.exec_command(command)

        serr = stderr.read()
        if serr:
            raise Exception(serr)

        sout = float(stdout.read())
        return sout

    def get_gyroscope(self):
        command = 'cat /sys/bus/iio/devices/iio:device*/in_anglvel_x_raw'
        stdin, stdout, stderr = self.ssh.exec_command(command)

        serr = stderr.read()
        if serr:
            raise Exception(serr)

        sout = float(stdout.read())
        return sout

