from paramiko.client import SSHClient, AutoAddPolicy


class PCBInit:
    def __init__(self, remote_ip, key):
        self.coefficient = 100 / 80
        self.ssh = SSHClient()
        self.ssh.set_missing_host_key_policy(AutoAddPolicy())
        self.ssh.connect(remote_ip, username="root", key_filename=key)

    def init_pcb(self, serialnum, version, date):
        command = "neuroeeprom -s " + serialnum + " -p " + version + " -d " + "\"" + date + "\""
        stdin, stdout, stderr = self.ssh.exec_command(command)

        serr = stderr.read()
        if serr:
            raise Exception(serr)

        return str(stdout.read())


