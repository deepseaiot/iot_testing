*** Keywords ***
Set Variables v4
    Set Suite Variable    $SOCI2CDEV    0
    Set Suite Variable    $SOCEEPROMADDRHEX    52
    Set Suite Variable    $RTCI2CDEV    1
    Set Suite Variable    $RTCADDRHEX    "57"
    Set Suite Variable    $BOARDI2CDEV    "1"
    Set Suite Variable    $SHTADDRHEX    "40"
    Set Suite Variable    $ACCLMTRI2CDEV    "0"
    Set Suite Variable    $ACCLMTRHEX    "68"
    Set Suite Variable    $HUMIDITYI2CDEV    "0"
    Set Suite Variable    $HUMIDITYHEX    "70"
    Set Suite Variable    $BOARDEEPROMADDRHEX    "54"


Set Variables v5
    Set Suite Variable    $SOCI2CDEV    1
    Set Suite Variable    $SOCEEPROMADDRHEX    52
    Set Suite Variable    $RTCI2CDEV    1
    Set Suite Variable    $RTCADDRHEX    "57"
    Set Suite Variable    $BOARDI2CDEV    "1"
    Set Suite Variable    $SHTADDRHEX    "40"
    Set Suite Variable    $ACCLMTRI2CDEV    "0"
    Set Suite Variable    $ACCLMTRHEX    "68"
    Set Suite Variable    $HUMIDITYI2CDEV    "1"
    Set Suite Variable    $HUMIDITYHEX    "70"
    Set Suite Variable    $BOARDEEPROMADDRHEX    "54"
Get Neuro version
    [Documentation]    Find neuro version
    [Arguments]    ${version}
    ${ver}=    Run Keyword If    '${version}' == '4.4'
    ...    Set Variable    4
    ...  ELSE IF    '${version}' == '5.5'
    ...    Set Variable    5
    ...  ELSE
    ...    Set Variable    unknown
    [Return]    ${ver}
Set Neuro Variables
    [Documentation]    Set system variables according to neuro version
    [Arguments]    ${version}
    Log    Version is ${version}
    ${neuro_version}=    Get Neuro version    ${version}
    Run Keyword If    ${neuro_version}==5
    ...    Set Variables v5
    ...  ELSE IF    ${neuro_version}==4
    ...    Set Variables v4
    ...  ELSE
    ...    Fail
