*** Settings ***
Library         SSHLibrary

*** Keywords ***
Write To EEPROM
    [Documentation]     Writes to EEPROM and verifies. Takes as arguments the serial number and version and manufacturing get current date to write to eeprom
    [Arguments]     ${serial}   ${version}    ${mfgdate}
    ${stdout}   ${stderr}   ${rc}=  execute command     neuroeeprom -s ${serial} -p ${version} -d "${mfgdate}"    return_stderr=True  return_rc=True
    should be empty    ${stderr}
    should be equal as integers    ${rc}      0
    Should Contain  ${stdout}   verified

Read EEPROM as Dictionary
    [Documentation]     Reads EEPROM and return ouput
    ${stdout}   ${stderr}   ${rc}=  execute command     neuroeeprom -c     return_stderr=True  return_rc=True
    &{output_dict}=      evaluate    json.loads('''${stdout}''')      json

    [Return]    ${output_dict}

Convert EEPROM Output To Json
    [Documentation]     Convert eeprom output string to json
    [Arguments]     ${in}

    &{new_dict}=      evaluate    json.loads('''${in}''')      json

    [Return]  &{new_dict}

Clear EEPROM
    ${stdout}   ${stderr}   ${rc}=  execute command     neuroeeprom -r     return_stderr=True  return_rc=True
    should be empty    ${stderr}
    should be equal as integers    ${rc}      0
