*** Settings ***
Documentation    Suite description
Library         SSHLibrary

*** Variables ***
${FUNCTIONS_PATH}   /lib/ds

*** Keywords ***
Login To Neuro
    [Arguments]     ${ip}  ${key}

    Open Connection     ${ip}
    login with public key  root    ${key}
    set client configuration    timeout=30s  prompt=#
    Write     /bin/bash
    ${out}=     read until prompt
    Write     FUNCTIONS_PATH=${FUNCTIONS_PATH}
    ${out}=     read until prompt
    Write     . $FUNCTIONS_PATH/init-functions
    ${out}=     read until prompt
    Write     . $FUNCTIONS_PATH/neuro-hardware-functions
    ${out}=     read until prompt
    Write     . $FUNCTIONS_PATH/eeprom-functions
    ${out}=     read until prompt
    Write     . $FUNCTIONS_PATH/rtc-functions
    ${out}=     read until prompt
    Write     . $FUNCTIONS_PATH/serial-port-functions
    ${out}=     read until prompt
    Write     . $FUNCTIONS_PATH/neuroled-test-resources
    ${out}=     read until prompt