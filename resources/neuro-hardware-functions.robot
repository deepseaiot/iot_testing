*** Settings ***
Library     String
Library         SSHLibrary

*** Keywords ***
Get Power Consumption
    ${stdout}   ${stderr}   ${rc}=  execute command     cat /sys/bus/i2c/devices/${SOCI2CDEV}-00${SHTADDRHEX}/hwmon/hwmon*/power1_input     return_stderr=True  return_rc=True
    should be empty    ${stderr}    msg="Power Monitor does not exist"
    should be equal as integers    ${rc}      0
    ${std_int}=     convert to integer    ${stdout}
    [Return]    ${stdout}

Get Proximity
    [Documentation]     Get proximity sensor value
    Write     get_proximity
    ${out}=     read until prompt
    ${proximity_line}=   split to lines  ${out}  0   -1
    Should not contain  ${proximity_line}   Proximity sensor device tree node does not exist on bus     msg=Device does not exists  values=False
    Should Not Be Equal As Strings  ${proximity_line}[0]    NaN     ${proximity_line}[0] is ot a valid value    values=false
    ${proximity}=   convert to integer  ${proximity_line}[0]

    [Return]    ${proximity}

Get Humidity
    [Documentation]     Get humidity sensor value
    ${stdout}   ${stderr}   ${rc}=  execute command     cat /sys/class/i2c-dev/i2c-${SOCI2CDEV}/device/${SOCI2CDEV}-00${HUMIDITYHEX}/hwmon/hwmon*/humidity1_input     return_stderr=True  return_rc=True
    should be empty    ${stderr}
    should be equal as integers    ${rc}      0
    ${std_int}=     convert to integer    ${stdout}
    [Return]    ${stdout}

Get Accelerometer
    [Documentation]     Get accelerometer
    Write     get-accel
    ${accel_line}=     read until prompt
    Should not contain  ${accel_line}   Accelerometer does not appear in linux devices     msg=Device does not exists  values=False
    Should not contain  ${accel_line}   No such file or directory   msg=error checking x/y/z values

Get Gyroscope
    [Documentation]     Get gyroscope
    Write     get-gyro
    ${accel_line}=     read until prompt
    Should not contain  ${accel_line}   Gyroscope does not appear in linux devices     msg=Device does not exists  values=False
    Should not contain  ${accel_line}   No such file or directory   msg=error checking x/y/z values

Get Temperature
    [Documentation]     Get temperature sensor value
    ${stdout}   ${stderr}   ${rc}=  execute command     cat /sys/class/i2c-dev/i2c-${SOCI2CDEV}/device/${SOCI2CDEV}-00${HUMIDITYHEX}/hwmon/hwmon*/temp1_input     return_stderr=True  return_rc=True
    should be empty    ${stderr}
    should be equal as integers    ${rc}      0
    ${std_int}=     convert to integer    ${stdout}
    [Return]    ${stdout}

Read Soc EEPROM
    [Documentation]     Read EEPROM
    Write     read-soc-eeprom
    ${out}=     read until prompt
    ${temperature_line}=   split to lines  ${out}  0   -1

Start Recording Button
    [Documentation]     Start recording button events
    Write   evtest /dev/input/event1>buttonoutput.txt &
    ${out}=     read until prompt
	Write   pid=$!
    ${out}=     read until prompt
	Write   echo $pid
	${out}=     read until prompt
    ${temp}=   split to lines  ${out}  0   -1
    ${pid}=   convert to integer  ${temp}[0]

    [Return]    ${pid}


Stop Recording Button
    [Documentation]     Stop recording button events
    [Arguments]     ${pid}
	Write   kill -9 ${pid}
    ${out}=     read until prompt
	Write   rm -f buttonoutput.txt
    ${out}=     read until prompt

Get Times Button Pressed
    [Documentation]     Get how many times button is pressed
	Write   grep "type 1 (EV_KEY), code 256 (BTN_0), value 1" buttonoutput.txt | wc -l
    ${out1}=     read until prompt
	Write   grep "type 1 (EV_KEY), code 256 (BTN_0), value 0" buttonoutput.txt | wc -l
    ${out2}=     read until prompt

    ${temp}=   split to lines  ${out1}  0   -1
    ${times_pressed}=   convert to integer  ${temp}[0]

    [Return]    ${times_pressed}

Write To SD Card
    [Documentation]     Write given argument string to SD Card.
    [Arguments]     ${in}

    ${stdout}   ${stderr}   ${rc}=  execute command     echo "${in}\n" | dd of=/dev/mmcblk1 status=none     return_stderr=True  return_rc=True


Read SD Card
    [Documentation]     Reads the first N bytes of SD card. Takes as argument the number of bytes to read
    [Arguments]     ${length}

    ${actual_length}=   evaluate    ${length}+1
    ${stdout}   ${stderr}   ${rc}=  execute command     dd if=/dev/mmcblk1 bs=${actual_length} count=1 status=none     return_stderr=True  return_rc=True
    should be empty    ${stderr}
    should be equal as integers    ${rc}      0
    ${sd_content}=   split to lines  ${stdout}  0   -1

    [Return]    ${stdout}

Get Random Alphanumeric
    [Documentation]     Return random alphanumeric string consisting of lowercase and/or uppercase ASCII characters and numbers.
    ...                 Takes as argument the lenght of the string to be generated
    [Arguments]     ${length}

    ${random_string}=   generate random string      ${length}   [LETTERS][NUMBERS]

    [Return]    ${random_string}
