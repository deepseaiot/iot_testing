*** Settings ***
Library         SSHLibrary

*** Keywords ***
Set Thermal Zone
    [Documentation]    Set temperature limit for specified thermal zone
    [Arguments]    ${temp}  ${trip}
    open Connection     ${REMOTE_IP}
    login with public key  root    ${KEYFILE}
    set client configuration    timeout=30s  prompt=#
    ${stdout}   ${stderr}   ${rc}=  execute command     echo ${temp} > /sys/class/thermal/thermal_zone0/trip_point_${trip}_temp     return_stderr=True  return_rc=True
    should be empty    ${stderr}
    should be equal as integers    ${rc}      0

Get Thermal Zone
    [Documentation]    Get temperature limit for specified thermal zone
    [Arguments]   ${trip}
    open Connection     ${REMOTE_IP}
    login with public key  root    ${KEYFILE}
    set client configuration    timeout=30s  prompt=#
    ${stdout}   ${stderr}   ${rc}=  execute command     cat /sys/class/thermal/thermal_zone0/trip_point_${trip}_temp     return_stderr=True  return_rc=True
    should be empty    ${stderr}
    should be equal as integers    ${rc}      0

    [Return]    ${stdout}