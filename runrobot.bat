@ECHO OFF
ECHO Running test cases
ECHO 
robot -P lib/ -d logs/ --loglevel DEBUG -v RESOURCES:../resources -v REMOTE_IP:%1 testsuites/%2.robot
PAUSE