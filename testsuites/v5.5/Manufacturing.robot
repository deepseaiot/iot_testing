*** Settings ***
Documentation   Manufacturing suite for Neuro testing and initialization
Suite Setup     Suite Setup
Suite Teardown  Suite Teardown
Library         SSHLibrary
Library         Collections
Library         DateTime
Library         Dialogs
#Library         PCBInit   ${REMOTE_IP}  ${KEYFILE}
Resource        ${RESOURCES}/neuro-hardware-functions.robot
Resource        ${RESOURCES}/neuro-login.robot
Resource        ${RESOURCES}/eeprom-keywords.robot
Resource        ${RESOURCES}/fan-keywords.robot
Resource        ${RESOURCES}/neuro_environment.robot
Force Tags    manufacturing    5.5


*** Variables ***
@{ALL_LEDS}     logo1   logo2   logo3   logo4   main-red    main-green  main-blue   main-white

*** Test Cases ***
Test EEPROM
    [Documentation]   Test EEPROM read/write. Write to EERPOM a dictionary. Expects to read the same dictionary
    [Tags]    automatic   eeprom
    [Timeout]  60s
    [Teardown]    Test EEPROM Teardown
    ${now}=     get current date    result_format=%m/%d/%m/%Y %H:%M
    #${eeprom_now}=     convert date  ${now}   result_format=%Y\\/%m\\/%d %H:%M
    ${version}=     set variable    2.0
    &{saved_output}=          Read EEPROM as Dictionary
    set test variable  &{saved_eeprom}  &{saved_output}
    Clear EEPROM
    &{source_dict}=     create dictionary   SerialNum=123456789000  PCBVersion=${version}    ProductionDateUTC=${now}
    &{eeprom_dict}=     create dictionary   EEPROM=${source_dict}
    Write To EEPROM     123456789000   ${version}     ${now}
    &{output}=          Read EEPROM as Dictionary
    dictionaries should be equal    ${eeprom_dict}  ${output}

Test USB
    [Documentation]  Check if usb dev path is present
    [Tags]    automatic     usb     v4.4     v5.5
    ${stdout}   ${stderr}   ${rc}=  execute command     lsusb|grep -e 0451:2036     return_stderr=True  return_rc=True
    should be empty    ${stderr}
    should be equal as integers    ${rc}      0
    Should Contain  ${stdout}   0451:2036


Test RTC
    [Documentation]  Test RTC. Test presence of rtc in linux devices.
    [Tags]     automatic    rtc
    ${stdout}   ${stderr}   ${rc}=  execute command     neurotest -t     return_stderr=True  return_rc=True
    #should be empty    ${stderr}
    should be equal as integers    ${rc}      0
    Should Not Contain  ${stdout}   failed

Test I2C
    [Documentation]   Test I2C. Check Presence of I2C bus in linux
    [Tags]    automatic   i2c
    ${stdout}   ${stderr}   ${rc}=      execute command     neurotest -i     return_stderr=True  return_rc=True
    should be empty    ${stderr}
    should be equal as integers    ${rc}      0
    Should Not Contain  ${stdout}   failed

Test Ethernet Port
    [Documentation]    Test Ethernet Port
    [Tags]    Sanity      automatic   ethernet
    ${stdout}   ${stderr}   ${rc}=      execute command     ifconfig eth0     return_stderr=True  return_rc=True
    should be empty    ${stderr}
    should be equal as integers    ${rc}      0
    Should Not Contain  ${stdout}   Device not found
    ${stdout}   ${stderr}   ${rc}=      execute command     cat /sys/class/net/eth0/speed    return_stderr=True  return_rc=True
    should be empty    ${stderr}
    should be equal as integers    ${rc}      0
    Should Contain  ${stdout}   1000

Test Button
    [Documentation]   Test Button
    [Tags]    manual    button

    ${pid}=     Start Recording Button
    ${user_selection}=      pause execution    Press button any number of times and then press OK:
    ${times_pressed}=   Get Times Button Pressed
    Stop Recording Button   ${pid}

    ${user_selection}=      Get Value From User     How many times did you press the tested button?:
    Should be equal as integers   ${times_pressed}    ${user_selection}

Test Serial Ports
    [Documentation]   Test Serial Ports communication. (Must be short circuited)
    [Tags]    manual  serial
    
    Check Serial Port   /dev/ttyMAX0    /dev/ttyMAX1    4800
    Check Serial Port   /dev/ttyMAX0    /dev/ttyMAX1    19200
    Check Serial Port   /dev/ttyMAX0    /dev/ttyMAX1    115200
    Check Serial Port   /dev/ttyMAX2    /dev/ttyMAX3    4800
    Check Serial Port   /dev/ttyMAX2    /dev/ttyMAX3    19200
    Check Serial Port   /dev/ttyMAX2    /dev/ttyMAX3    115200
    Check Serial Port   /dev/ttyAMA0    /dev/ttyAMA1    4800
    Check Serial Port   /dev/ttyAMA0    /dev/ttyAMA1    19200
    Check Serial Port   /dev/ttyAMA0    /dev/ttyAMA1    115200

Test Power Monitor
    [Documentation]     Check power monitor
    [Tags]    automatic     hwmon    

    ${power}=   Get Power Consumption
    ${power}=   evaluate    ${power}/1000000
    Should Be True  ${power}>=3.6  msg=Power(${power}) too low.
    Should Be True  ${power}<=11  msg=Power(${power}) too high.

Test Fan
    [Documentation]   Test Fan
    [Tags]    fan     manual
    [Teardown]  Fan Test Teardown

    ${cur_trip}    get thermal zone    0
    set test variable    ${saved_trip}  ${cur_trip}
    set thermal zone    10000   0
    ${user_selection}=      execute manual step     Is fan working?

Test TPM
    [Documentation]   Test TPM Secutity chip
    [Tags]    tpm     automatic

    ${serial_count}   ${stderr}   ${rc}=  execute command     cat /sys/class/tpm/tpm0/dev   return_stderr=True    return_rc=True
    should be empty    ${stderr}    msg="TPM does not exist"
    should be equal as integers    ${rc}      0     msg="RC error"

*** Keywords ***
Suite Setup
    #Login To Neuro  ${REMOTE_IP}    ${KEYFILE}
    Login To Neuro  ${REMOTE_IP}    ${KEYFILE}
    Set Neuro Variables    ${VERSION}

Test EEPROM Teardown
    #${converted_date}=  convert date  ${saved_eeprom['EEPROM']['ProductionDateUTC']}    result_format=%m/%d/%m/%Y %H:%M
    run keyword if    'EEPROM' in ${saved_eeprom}      Clear EEPROM
    run keyword if    'EEPROM' in ${saved_eeprom}      Write To EEPROM      ${saved_eeprom['EEPROM']['SerialNum']}    ${saved_eeprom['EEPROM']['PCBVersion']}      ${saved_eeprom['EEPROM']['ProductionDateUTC']}

Suite Teardown
    Close Connection

Reset Connection
    close connection
    Login To Neuro  ${REMOTE_IP}    ${KEYFILE}


Set Led
    [Arguments]     ${led}  ${trigger}  ${brigtness}=0
    Log     ${led}
    Log     ${trigger}
    Log     ${brigtness}
    run keyword if  '${trigger}'=='off'     Write     neuroleds -l ${led} -b ${brigtness} -t ${trigger}
    ...     ELSE    Write     neuroleds -l ${led} -b ${brigtness} -t ${trigger}
    ${output}=    read until prompt

Get Data From User
    [Documentation]  Get version and serial number from user
    ${version}=     Get Value From User     Version (X.X):
    set suite variable  ${VERSION}  ${version}
    ${serialnum}=     Get Value From User     Serial Nuber (12 digits):
    set suite variable  ${SERIALNUM}  ${serialnum}

Test Setup Leds
    Set Led     logo1   off
    Set Led     logo2   off
    Set Led     logo3   off
    Set Led     logo4   off
    Set Led     main-red   off
    Set Led     main-green   off
    Set Led     main-blue   off
    Set Led     main-white   off

Fan Test Teardown
    Set Thermal Zone    ${saved_trip}   0

Check Serial Port
    [Arguments]     ${source}  ${dest}  ${baudrate}
    ${stdout}   ${stderr}   ${rc}=  execute command     pyserialtest ${source} ${dest} ${baudrate}     return_stderr=True  return_rc=True
    should be empty    ${stderr}    "Error testing ${source} to ${dest}"
    should be equal as integers    ${rc}      0    "Error testing ${source} to ${dest}"
    Should Contain  ${stdout}   Data match
    ${stdout}   ${stderr}   ${rc}=  execute command     pyserialtest ${source} ${dest} ${baudrate}     return_stderr=True  return_rc=True
    should be empty    ${stderr}
    should be equal as integers    ${rc}      0     msg="RC error"
    Should Contain  ${stdout}   Data match
