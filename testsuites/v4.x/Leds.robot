*** Settings ***
Documentation    Test Suite for LED Testing
Suite Setup     Suite Setup
Suite Teardown  Suite Teardown
Library         SSHLibrary
Library         Dialogs
Resource        ${RESOURCES}/neuro-login.robot


*** Variables ***
@{ALL_LEDS}     logo1   logo2   logo3   logo4   main-red    main-green  main-blue   main-white


*** Test Cases ***
Test leds on_off
    [Documentation]     Test "on" state in all leds
    [Tags]    logo_leds     manual  manufacturing
    FOR     ${led}  IN  @{ALL_LEDS}
        Set Led     ${led}   on      100
        ${user_selection}=      execute manual step     Is ${led} led on?
        Set Led     ${led}   off
    END

Test leds blink
    [Documentation]     Test "blink" state in all leds
    [Tags]    logo_leds     manual  manufacturing   optional
    FOR     ${led}  IN  @{ALL_LEDS}
        Set Led     ${led}   blink      100
        ${user_selection}=      execute manual step     Is ${led} led blinking?
        Run Keyword if      'logo' in '''${led}'''  Set Led     ${led}   off    100
        Set Led     ${led}   off    100
    END

Test leds fade
    [Documentation]     Test fade in/out in all leds
    [Tags]    logo_leds     manual  manufacturing   optional
    FOR     ${led}  IN  @{ALL_LEDS}
        Set Led     ${led}   fade      100
        Set Led     ${led}   fade      0
        ${user_selection}=      execute manual step     Did ${led} led fade in/out?
        Set Led     ${led}   off
    END

*** Keywords ***
Suite Setup
    Login To Neuro  ${REMOTE_IP}    ${KEYFILE}
    Set Led     logo1   off
    Set Led     logo2   off
    Set Led     logo3   off
    Set Led     logo4   off
    Set Led     main-red   off
    Set Led     main-green   off
    Set Led     main-blue   off
    Set Led     main-white   off

Suite Teardown
    Close Connection

Set Led
    [Arguments]     ${led}  ${trigger}  ${brigtness}=0
    Log     ${led}
    Log     ${trigger}
    Log     ${brigtness}
    run keyword if  '${trigger}'=='off'     Write     neuroleds -l ${led} -b ${brigtness} -t ${trigger}
    ...     ELSE    Write     neuroleds -l ${led} -b ${brigtness} -t ${trigger}
    ${output}=    read until prompt

