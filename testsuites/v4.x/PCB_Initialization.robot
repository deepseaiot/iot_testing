*** Settings ***
Documentation   Neuro Initialization
Library         SSHLibrary
Library         DateTime
Library         Collections
Resource        ${RESOURCES}/eeprom-keywords.robot
Resource        ${RESOURCES}/neuro-login.robot
Suite Setup    Suite Setup
Suite Teardown    Suite Teardown
*** Test Cases ***
PCB Initialization
    [Documentation]  Set Serial number, Version number, Manufacturing date and MAC address to neuro
    [Tags]    init
    ${now}=     get current date    result_format=%Y/%m/%d %H:%M    time_zone=UTC
    ${eeprom_now}=     convert date  ${now}   result_format=%Y\\/%m\\/%d %H:%M
    ${eval_serial}=     evaluate  '{:0>7}'.format(${SERIALNUM})
    &{source_dict}=     create dictionary   SerialNum=${eval_serial}  PCBVersion=${VERSION}      ProductionDateUTC=${now}
    &{eeprom_dict}=     create dictionary   EEPROM=${source_dict}
    Clear EEPROM
    Write To EEPROM     ${SERIALNUM}   ${VERSION}     ${eeprom_now}
    &{output}=          Read EEPROM as Dictionary
    dictionaries should be equal    ${eeprom_dict}  ${output}
    Set MAC

*** Keywords ***
Suite Setup
    Login To Neuro  ${REMOTE_IP}    ${KEYFILE}

Suite Teardown
    close connection

Set MAC
    [Documentation]  Set MAC address
    ${stdout}   ${stderr}   ${rc}=  execute command     setEEPROM9514.sh -m ${MAC}     return_stderr=True  return_rc=True
    should be empty    ${stderr}
    should be equal as integers    ${rc}      0
    Should Contain  ${stdout}   Setting MAC OK