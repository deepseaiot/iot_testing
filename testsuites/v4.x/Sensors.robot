*** Settings ***
Documentation    Test Suite for sensors testing
Suite Setup     Suite Setup
Suite Teardown  Suite Teardown
Library         SSHLibrary
Library         Dialogs
#Library         Sensors   ${REMOTE_IP}  ${KEYFILE}
Resource    ../../resources/neuro-hardware-functions.robot
Resource    ../../resources/neuro-login.robot
Resource    ../../resources/neuro_environment.robot
Force Tags  sensors

*** Test Cases ***
Test Humidity Sensor
    [Documentation]   Test humidity sensors.
    [Tags]    automatic   sensors   humidity        manufacturing
    ${humidity}=     Get Humidity
    Validate Humidity  ${humidity}

Test Temperature Sensor
    [Documentation]   Test temperature sensors.
    [Tags]    Sanity      automatic   sensors   temperature     manufacturing
    ${temperature}=     Get Temperature
    Validate Temperature  ${temperature}

Test Proximity Sensor
    [Documentation]   Test proximity sensor.
    [Tags]    Sanity      automatic   sensors   proximity       manufacturing
    ${proximity}=     Get Proximity
    Validate Proximity  ${proximity}

Test Accelerometer Sensor
    [Documentation]   Test accelerometer sensor.
    [Tags]    automatic   sensors   accelerometer       manufacturing
    ${stdout}   ${stderr}   ${rc}=  execute command     cat /sys/bus/iio/devices/iio:device*/in_accel_x_raw     return_stderr=True  return_rc=True
    should be empty    ${stderr}
    should be equal as integers    ${rc}      0


Test Gyroscope Sensor
    [Documentation]   Test gyroscope sensor.
    [Tags]      automatic   sensors   gyroscope       manufacturing
    ${stdout}   ${stderr}   ${rc}=  execute command     cat /sys/bus/iio/devices/iio:device*/in_anglvel_x_raw     return_stderr=True  return_rc=True
    should be empty    ${stderr}
    should be equal as integers    ${rc}      0

*** Keywords ***
Suite Setup
    Login To Neuro  ${REMOTE_IP}    ${KEYFILE}
    Set Neuro Variables    ${VERSION}

Suite Teardown
    Close Connection

Validate Humidity
    [Arguments]     ${humidity}
    ${humidity_int}=   convert to integer  ${humidity}
    Should Be True  ${humidity_int}>=1000  msg=Humidity(${humidity_int}) too low. Limit is 1000
    Should Be True  ${humidity_int}<=80000  msg=Humidity(${humidity_int}) too high. Limit is 80000

Validate Temperature
    [Arguments]     ${temperature}
    ${temperature_int}=   convert to integer  ${temperature}
    Should Be True  ${temperature_int}>=10000   msg=Temperature(${temperature_int}) too low. Limit is 10000
    Should Be True  ${temperature_int}<=50000   msg=Temperature(${temperature_int}) too high Limit is 50000

Validate Proximity
    [Arguments]     ${proximity}
    ${proximity_int}=   convert to integer  ${proximity}
    Should Be True  ${proximity_int}>=1  msg=Proximity(${proximity_int}) too low. Limit is 1
    #Should Be True  ${proximity_int}<=7000  msg=Proximity(${proximity_int}) too high. Limit is 6000