from paramiko.client import SSHClient
from paramiko import RSAKey, AutoAddPolicy
import json, sys

print(sys.argv)
server = sys.argv[1]
key = sys.argv[2]

try:
    client = SSHClient()

    client.set_missing_host_key_policy(AutoAddPolicy())
    k = RSAKey.from_private_key_file(key)
    client.connect(server, username='root', pkey=k)

    stdin, stdout, stderr = client.exec_command('setEEPROM9514.sh -p')

    f = stdout.read()
    tt = f.split(b':', 1)[1]
    json_str = str(tt.decode('utf-8', 'ignore'))
    print(json_str.strip().replace(' ', ':'))
except Exception as e:
    print("undefined")
