@ECHO OFF
setlocal enabledelayedexpansion
goto :check_args

:main
ECHO.
ECHO ------------------------------
ECHO Manufacturing tests and set up
ECHO ------------------------------
ECHO.
set remote_ip=%1
set suites=%2
set serialnum=default
set KEY=%3

if "%2"=="init" (
    call :init_data
    goto :resume_info
) else (
    goto :check_init
)

:resume_info
call :serialnum_warning %serialnum% %version% %mac_address%

if "%2"=="init" (
    set type_tag=-i init
    call :run_tests %2
)
if "%2"=="manual" (
    set exclude_optional=-e optional
    set type_tag=-i manual
    call :run_tests %2
)
if "%2"=="self-tests" (
    set type_tag=-i automatic
    call :run_tests %2
)
if "%2"=="all" (
    set type_tag=-i manufacturing
    set exclude_optional=-e optional
    call :run_tests %2
)

goto :EOF

:run_tests
echo.
echo Executing Testcases...
echo.

robot -P lib/ -d logs/%serialnum%/%1 --log %serialnum%_log.html --report %serialnum%_report.html --loglevel DEBUG -v RESOURCES:../resources %type_tag% %init_data_tag% %exclude_optional% -v REMOTE_IP:%remote_ip% -v SERIALNUM:%serialnum% -v KEYFILE:%KEY% -v VERSION:%version% -v MAC:%mac_address% testsuites/Manufacturing.robot
exit /b 0

:init_data
echo.
call :set_serial
call :set_version
call :set_mac
echo.
exit /b 0

:set_serial
set /P serialnum="Serial number (7digits): "
echo !serialnum!|findstr /r "^[0123456789][0123456789][0123456789][0123456789][0123456789][0123456789][0123456789]$" > nul || (
  echo Invalid serial number. Try again.
  goto :set_serial
)
exit /b 0

:set_version
set /P version="PCB Version (X.X or XX.X): "
echo !version!|findstr /r "^[0123456789].[0123456789]$ ^[0123456789][0123456789].[0123456789]$" > nul || (
  echo Invalid version format. Try again.
  goto :set_version
)
exit /b 0

:set_mac
set /P mac_address="MAC Address (xx:xx:xx:xx:xx:xx): "
echo !mac_address!|findstr /ri "^[0123456789ABCDEF][0123456789ABCDEF]:[0123456789ABCDEF][0123456789ABCDEF]:[0123456789ABCDEF][0123456789ABCDEF]:[0123456789ABCDEF][0123456789ABCDEF]:[0123456789ABCDEF][0123456789ABCDEF]:[0123456789ABCDEF][0123456789ABCDEF]*$" >nul || (
  echo Invalid mac address format. Try again.
  goto :set_mac
)
exit /b 0

:check_args
set argC=0
for %%x in (%*) do Set /A argC+=1

if not %argC%==3 (
    echo.
    echo Wrong arguments...
    goto :usage
) else (
    goto :check_suite
)

:check_key
if not exist %3 (
    echo Key file "%3" not found...
    goto :EOF
)
goto :main

:check_suite
if "%2"=="manual" goto :check_key
if "%2"=="self-tests" goto :check_key
if "%2"=="all" goto :check_key
if "%2"=="init" goto :check_key
goto :usage

:usage
echo.
echo Usage: runmfgsuite ^<remote ip^> ^<test suite^> ^<ssh_key_file^>
echo   remote_ip: the remote ip of the pcb to test
echo   test suite: valid options [init, manual, self-tests, all]
echo        init: Run only the initialization procedure
echo        manual: Run only manual tests with optional initialization.
echo        self-tests: Run only self-tests with optional initialization.
echo        all: Run both manual and self-tests with optional initialization.
echo   ssh_key_file: ssh public key for login to the remote ip
echo.
goto :EOF

:serialnum_warning
echo.
echo ----------------------
echo   Current EEPROM Data
echo ----------------------
echo PCB Serial Number: %1
echo PCB version: %2
echo MAC Address: %3
echo.
exit /b 0

:getserial
python get_eeprom_serial.py %remote_ip% %key%> tmpSerial
for /F %%F in ('type tmpSerial') do (
    set tmpvar=%%F
)
if not "!tmpvar!"=="undefined" (
    set serialnum=!tmpvar!
)
del tmpSerial
exit /b 0

:getversion
python get_eeprom_version.py %remote_ip% %key%> tmpVer
for /F %%F in ('type tmpVer') do (
    set tmpvar=%%F
)
if not "!tmpvar!"=="undefined" (
    set version=!tmpvar!
)
del tmpVer
exit /b 0

:getmac
python get_mac.py %remote_ip% %key%> tmpMac
for /F %%F in ('type tmpMac') do (
    set tmpvar=%%F
)
if not "!tmpvar!"=="undefined" (
    set mac_address=!tmpvar!
)
del tmpMac
exit /b 0

:check_init
set /P ans_init="Initialize pcb data?(Y/N): "
echo.
if /I "%ans_init%"=="Y" (
    call :init_data
    set init_data_tag=-i init
    goto :resume_info
)
if /I "%ans_init%"=="N" (
    call :getserial
    call :getversion
    call :getmac
    goto :resume_info
)
goto :check_init
