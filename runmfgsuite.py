#!/usr/bin/env python3
import os, sys, json, re
from paramiko import SSHClient, AutoAddPolicy


def usage():
    print("\n")
    print("Usage: runmfgsuite ^<remote ip^> ^<test suite^> ^<ssh_key_file^>")
    print("\n")
    print("remote_ip: the remote ip of the pcb to test")
    print("\n")
    print("test suite: valid options [init, manual, self-tests, all, leds, sensors]")
    print("\tinit: Run only the initialization procedure")
    print("\tmanual: Run only manual tests with optional initialization.")
    print("\tself-tests: Run only self-tests with optional initialization.")
    print("\tall: Run both manual and self-tests with optional initialization.")
    print("\tleds: Run only led tests.")
    print("\tsensors: Run only sensors tests.")
    print("\tserial: Run only serial port tests.")
    print("\n")
    print("ssh_key_file: ssh public key for login to the remote ip")
    print("\n")
    sys.exit(0)


def get_eeprom(client):
    stdin, stdout, stderr = client.exec_command('neuroeeprom -c')
    eeprom = {}
    f = stdout.read()
    if f and f != b"\n":
        tt = f.split(b'\x00', 1)[0]
        json_str = str(tt.decode('utf-8', 'ignore'))
        eeprom = json.loads(json_str)
    return eeprom


def get_mac(client):
    stdin, stdout, stderr = client.exec_command('setEEPROM9514.sh -p')
    f = stdout.read()
    tt = f.split(b':', 1)[1]
    json_str = str(tt.decode('utf-8', 'ignore')).strip().replace(' ', ':')
    return json_str


def init_pcb():
    serialnum = input("Serial number (7digits): ")
    version = input("PCB Version (X.X or XX.X): ")
    mac_address = input("MAC Address (xx:xx:xx:xx:xx:xx): ")

    return serialnum, version, mac_address


def print_info(eeprom, mac_addr):
    serialnum = eeprom["EEPROM"]["SerialNum"] if bool(eeprom) else ""
    version = eeprom["EEPROM"]["PCBVersion"] if bool(eeprom) else ""
    print("\n")
    print("---------------------")
    print("Current EEPROM Data")
    print("---------------------")
    print("PCB Serial Number: {}".format(serialnum))
    print("PCB Version Number: {}".format(version))
    print("MAC Address: {}".format(mac_addr))
    print("\n")


def get_params_from_suite(suite):
    run_init = False
    run_suites = ""
    tags = ""

    # Set robot tags
    if suite == "init":
        run_init = True
    elif suite == "manual":
        tags += " -i manual"
        run_suites = "*"
    elif suite == "self-tests":
        tags += " -i automatic"
        run_suites = "*"
    elif suite == "all":
        run_suites = "*"
        tags += " -i manufacturing"
    elif suite == "sensors":
        run_suites = "Sensors.robot"
    elif suite == "leds":
        run_suites = "Leds.robot"
    elif suite == "serial":
        tags += " -i serial"
        run_suites = "Manufacturing.robot"

    return run_suites, tags, run_init

def get_run_dir(version):
    return '5.5/' if re.match('5\..*', version) else '4.x/'

def intro():
    print("\n")
    print("------------------------------")
    print("Manufacturing tests and set up")
    print("------------------------------")
    print("\n")

def get_robot_variables(serialnum, version, mac_address, key, remote_ip):
    variables = ""

    version_str = (" -v VERSION:" + version) if version else " "
    serial_str = (" -v SERIALNUM:" + serialnum) if serialnum else " "
    mac_str = (" -v MAC:" + mac_address) if mac_address else " "

    variables += " -v RESOURCES:../../resources"
    variables += " -v KEYFILE:" + key
    variables += " -v REMOTE_IP:" + remote_ip
    variables += version_str + serial_str + mac_str

    return variables

def execute_tests(serialnum, suite, variables, run_dir, tags, run_suites):
    # Run robot
    command = "robot -P lib/ -d logs/" + serialnum + "/" + suite + \
        " --log " + serialnum + "_log.html" + \
        " --report " + serialnum + "_report.html" + \
        " --loglevel DEBUG" + \
        variables + \
        tags + \
        " testsuites/v" + run_dir + run_suites
    os.system(command)

def execute_init(serialnum, suite, variables, run_dir):
    command = "robot -P lib/ -d logs/" + serialnum + "/" + suite + " --log " + serialnum + "_log.html" + \
        " --report " + serialnum + "_report.html" + \
        " --loglevel DEBUG" + \
        variables + \
        " testsuites/v" + run_dir + "PCB_Initialization.robot"
    os.system(command)

def main(argv):
    if not argv or len(argv) < 3:
        usage()

    remote_ip = argv[0]
    suite = argv[1]
    key = argv[2]
    serialnum = "0000000"
    version = None
    mac_address = None
    variables = ""

    intro()

    ssh = SSHClient()
    ssh.set_missing_host_key_policy(AutoAddPolicy())
    ssh.connect(remote_ip, username="root", key_filename=key)
    eeprom = get_eeprom(ssh)
    mac_address = get_mac(ssh)
    if bool(eeprom):
        serialnum = eeprom["EEPROM"]["SerialNum"]
        version = eeprom["EEPROM"]["PCBVersion"]

    print_info(eeprom, mac_address)

    run_suites, tags, run_init = get_params_from_suite(suite)

    # Do not run optional test cases
    tags += " -e optional"

    # Check if there is a missing info from the init data. Idf so ask user to run init_pcb
    if (not version or not mac_address or serialnum == "0000000") and not run_init:
        ans = input("PCB is not initialized properly. Initialize pcb data now?(Y/N): ")    
        if ans.lower() == 'y':
            run_init = True
    
    if run_init:
        # Get serial number, version and mac address from user
        serialnum, version, mac_address = init_pcb()

    # get directory of the test suites to run depending on pcb version
    run_dir = get_run_dir(version)
    
    # Set robot variables
    variables = get_robot_variables(serialnum, version, mac_address, key, remote_ip)

    # Execute PCB initialization suite
    if run_init:
        execute_init(serialnum, suite, variables, run_dir)
    
    # Execute PCB tests
    if run_suites:
        execute_tests(serialnum, suite, variables, run_dir, tags, run_suites)

    ssh.close()


if __name__ == "__main__":
    main(sys.argv[1:])
    sys.exit(0)

